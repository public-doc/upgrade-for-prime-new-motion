
2025-02-18:
Stop using

Please go to the new addres and use the new file to update motion

new address: https://bitbucket.org/primerobotics/robot_system_update_repository/src/main/


Starting from 2025, it will stop being used. For updates, please visit the new addres:
https://gitlab.com/public-doc/robot_system_update_repository



V2.5.9
Release date: 2024-12-13
this folder only use for system ubuntu18.04


V2.5.8
Release date: 2024-11-01
this folder only use for system ubuntu18.04


V2.5.7
Release date: 2024-09-03
this folder only use for system ubuntu18.04


V2.5.6.1
Release date: 2024-08-01
only use for system ubunut18.04 


V2.5.6
Release date: 2024-07-19
only use for system ubunut18.04 


V2.5.5
Release date：2024-06-13


V2.5.4
Release date: 2024-04-24


V2.5.3
Release date: 2024-03-28


V2.5.2.1
Release date: 2024-03-15


V2.5.2
Release date:2024-02-29


V2.5.1
Release date:2024-01-19



V2.5.0
Release date: 2023-12-08

new feature：

ROB-334: add the REPALNPATHRESP cmd in mx bridge   增加replan响应协议--解决RCS和motion在通讯时间差的任务处理问题，避免因replan导致的抢占路径撞车问题

ROB-332: add the bettery soc record   增加电池电量记录			

ROB-330: modify the error code message accroding to pcb error code and add handshake with PCB   增加PCB握手协议-- 修改PCB的报警注释，增加握手协议防止通讯失联后电机按照原来速度行驶

ROB-329-1: modified the actuator state lift   重定义执行机构举升状态，避免信号干扰		

ROB-328: add hand shaking when rackbot is in elevator   增加进电梯握手协议-- 机器人进去电梯停稳后再允许电梯提升

ROB-327: modify motion version update script in start_up to use new repository   重新定义更新方式 更新文件更新，适用于选择版本的更新方式

ROB-326: Add support to DataLogic's barcode scanner 支持货架扫码		

ROB-313: add imu exception check and sudden rotation check   增加imu数据检查	

ROB-304: refine the shelf angle, fix the shelf spin action__rotate the shelf to the target angle 重定义货架角度，shelfspin旋转至目标角度"	

ROB-301: PCB release the button state in every state change PCB更新按钮状态

ROB-281: provide choice to calculate speed by encoder and add parameter rpmToDriver to support mp4400 增加编码器计算速度方式以支持MP4400"	

bugfix:

ROB-331: fix the search distance bug in center on shelf   修复centeronshelf检测距离的bug	

ROB-319: add a fix to query actuator state service in control node   修复查询执行机构状态失败的问题		

ROB-318: ignore the recharge in error when in recharge in pose   在充电位忽略充电失败的错误			

ROB-317: stop the actuator actions when emergency stop   收到FM急停时停止执行机构任务				

ROB-315: fix bag file size too big issue   修复bag文件过大的问题									

ROB-314: add restriction to update configure parameters when robot is in running state   机器人运行中限制更新参数														


V2.4.4
Release date: 2023-11-01

Features & Improvement
When robot recovers from error state, motion will change to ENGINEERING mode first 错误状态重启保持在工程模式

Rackbot:
Add separated charging parameter tuning for rackbot 对穿梭车的不同充电桩使用不同的充电参数

Add separated obstacle detection feature for storage lane and cross aisle 穿梭车可以独立使用storagelane和crossa isle方向的避障功能


Bug Fix
Fix micromove doesn’t work when robot has error but not from PCB 修复错误状态（非电机，硬件故障等）下无法微移动的问题

Other small fix when testing before release v2.4.4 

Rackbot:
Fix issue when quit recharge and center on code 修复退出充电移动时找码的bug

Fix issue when rackbot try to find the code 修复正常移动停止后的找码功能

Fix center on shelf issue 修复查找货架的功能

MX bot:
After enter grid finished, if the robot can’t see the code, it should not try to find the code 
切换地图时，不使用停止后找码功能；切换地铁失败时，recover后可以上报位置

Fix initial jerk in actuator spin issue 修复执行机构旋转初始速度过大的问题


V2.4.3
Release date: 2023-09-27

Features & Improvment
1. Consider build warn as error to add more restrication to code and avoid potential bug 

2. Adapt max path number sent from RCS dynamically 适应RCS动态修改最大路径数

3. Use dynamic slow down distance according to robot speed when robot encounters obstacles to avoid robot starts to slow down too early 避障时动态减速

4. Add PCB firmware version to log file when startup 开机时motion获取PCB的版本号

5. Change robot maximum speed limitation to 1.8m/s to Rackbot and 1.5m/s to MX robots 提速rackbot1.8m/s,mx 1.5m/s

6. Refine robot error code for different scenario to make them more clear to users 重定义错误码列表

Bug Fix
1. Fix can’t switch state when robot reboots from error state 修复开机时故障状态不能切换模式的问题

2. Fix issues when robot receives recharge in and recharge out command 修复自动充电/退出充电的异常的问题（需要RCS更新相关设置）

3. Fix robot won’t stop when chassis has no new control command from control node TESTING，增加control node异常报错

4. Add error report if chassis has no response after 5 seconds TESTING,增加PCB无反馈电机速度时报错

5. Fix actuator will not response actions if emergency stop command is sent after recover 修复RCS emergency stop和recover指令异常之后执行机构无法继续的问题

6. Fix robot can’t do back to normal when there is other actions sent to actuator 修复执行机构异步指令顺序的问题

7. Other bug fixes when tests this release 测试中遇到其他问题

Rackbot:

Fix known issues of obstacle detection after speed up to 1.8m/s TESTING 修复提速与避障测试中遇到问题


V2.4.2
Release date: 2023-09-06

Features
Rackbot:
The first relative stable version for Rackbot ROB-215: Rackbot feature development and bug fix in TTi  稳定的穿梭车版本

MX:
Use shell script to upload deployment files automatically and refine upgrade shell script to make it easier to deploy or update motion code. ROB-275: Write shell script to upload deployed files automatically and refine upgrade shell script 
请暂时忽略此条目，依旧使用之前的更新方式（新的更新方式需要的文件需要现有旧的更新方式下载到设备中），留待下次更新使用

Bug Fix
MX:
Fix robot can’t recover from suspend when robot is stopped by instant obstacle when tracking the current path. ROB-269: replan bug
修复在码上运行避障时，无法退出任务的bug

Fix robot will rotate slowly after startup or reboot. ROB-265: Robot rotated at a small speed after the reboot
修复开机启动时机器人缓慢移动的bug

Fix a bug for s curve speed generator when finding the new target speed and jerk. ROB-256: Robot stops with a very high speed, and report offcode error, because it can't find the code  修复速度发生器的bug

Add obstacle checking and use distance to find code when robot stops not on code. ROB-271: Check and find code need consider the shortest distance between two robots  找码时考虑避障信息

Fix actuator node crashes cause by bad code from shelf camera ROB-267: Integration testing for release of v2.4.2
修复货架码乱码时执行机构崩溃的问题

Other small fixes.

    

